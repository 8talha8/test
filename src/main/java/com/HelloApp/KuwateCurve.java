package com.HelloApp;

import java.util.*;

public class KuwateCurve {


		static void findCurve(List<Integer> lst) 
		{ 

			int size= lst.size();
			ArrayList<Integer>[] LIS = getSubSeq(lst, size); 
			Collections.reverse(lst);
			ArrayList<Integer>[] LDS = getSubSeq(lst, size); 
			
			int max = 0; 
			int maxIndex = -1; 
			int maxIndexj = -1; 
			for (int i = 0,j=size-1; i < size; i++,j--) 
			{ 
				if (LIS[i].size() + LDS[j].size() - 1 > max) 
				{ 
					max = LIS[i].size() + LDS[j].size() - 1; 
					maxIndex = i; 
					maxIndexj= j;
				} 
			} 
			System.out.println(maxIndex +"max"+ max);

			LIS[maxIndex].remove(LIS[maxIndex].size() - 1);
			Collections.reverse(LDS[maxIndexj]);
			LIS[maxIndex].addAll(LDS[maxIndexj] );
			for (int i = 0; i < LIS[maxIndex].size(); i++) 
				System.out.print(LIS[maxIndex].get(i) + " "); 

			System.out.println();
		}

		private static ArrayList<Integer>[] getSubSeq(List<Integer> lst, int n) {
			
			@SuppressWarnings("unchecked") 
			ArrayList<Integer>[] lSeq = new ArrayList[n]; 

			for (int i = 0; i < n; i++) 
				lSeq[i] = new ArrayList<>(); 

			lSeq[0].add(lst.get(0)); 

			for (int i = 1; i < n; i++) 
			{ 

				for (int j = 0; j < i; j++) 
				{ 

					if ((lst.get(i) > lst.get(j)) && 
						lSeq[j].size() > lSeq[i].size()) 
					{ 
						for (int k : lSeq[j]) 
							if (!lSeq[i].contains(k)) 
								lSeq[i].add(k); 
					} 
				} 
				lSeq[i].add(lst.get(i)); 
			}
			return lSeq;
		} 
 
		public static void main(String[] args) throws InterruptedException 
		{ 
			Integer[] arr = {0, 0, 0, 1, 1, 2, 1, 0, 0}; 
			
			List<Integer> l1 = new ArrayList<Integer>();
			Collections.addAll(l1, arr); 

			findCurve(l1); 
			
			Integer[] arr2 = {0, 1, 2, 3, 1, 0, 1, 2, 4, 2, 0}; 
			
			List<Integer> l2 = new ArrayList<Integer>();
			Collections.addAll(l2, arr2); 

			findCurve(l2); 
			
			Integer[] arr3 =  {0, 1, 2, 4, 1, 1, 0, 2, 3, 4, 3, 2, 0};
			
			List<Integer> l3 = new ArrayList<Integer>();
			Collections.addAll(l3, arr3); 

			findCurve(l3); 
			

		
		} 
	} 



