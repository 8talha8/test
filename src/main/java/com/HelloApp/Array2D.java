package com.HelloApp;


import java.io.*;
import java.util.*;

public class Array2D {
	static int minParkingSpaces(int[][] parkingStartEndTimes) {
	LinkedList<Integer[][]>l =new LinkedList<Integer[][]>();
	int x=0;
    for(int i=0;i<parkingStartEndTimes.length-1;i++){
			  
		for(int k=i+1;k<parkingStartEndTimes.length;k++){

		if(parkingStartEndTimes[i][0]<parkingStartEndTimes[k][0] && parkingStartEndTimes[i][1]<parkingStartEndTimes[k][0] && parkingStartEndTimes[k][0]!=-1){
		x++;		
		parkingStartEndTimes[k][0]=-1;
	
		}else if(parkingStartEndTimes[i][0]>parkingStartEndTimes[k][1] && parkingStartEndTimes[k][0]!=-1){
		x++;
		parkingStartEndTimes[k][0]=-1;
		}

		}
	}
	return x;
	}

	// DO NOT MODIFY ANYTHING BELOW THIS LINE!!

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter wr = new PrintWriter(System.out);
		int n = Integer.parseInt(br.readLine().trim());
		int[][] parkingStartEndTimeList = new int[n][2];
		String[] parkingStartEndTimes = br.readLine().split(" ");
		for (int i = 0; i < n; i++) {
			String[] parkingStartEndTime = parkingStartEndTimes[i].split(",");
			for (int j = 0; j < parkingStartEndTime.length; j++) {
				parkingStartEndTimeList[i][j] = Integer.parseInt(parkingStartEndTime[j]);
			}
		}

		int out = minParkingSpaces(parkingStartEndTimeList);
		System.out.println(out);

		wr.close();
		br.close();
	}
}