package com.HelloApp;


	 
	public class MethodHidding2 {
	 
	    public static void main(String[] args) {
	        Parent p = new Parent();
	        Parent c = new Child();
	        Child c2 = new Child();
	        
	        System.out.println("****************Method Hiding*******************");
	        p.foo(); // This will call method in parent class
	        c.foo(); // This will call method in parent class
	        c2.foo();
	       
	        System.out.println("****************Method overriding*******************");
	        p.bar(); // This will call method in parent class
	        c.bar(); // This will call method in child class
	        c2.bar();
	        Child c3 = (Child) new Parent();
	        c3.foo();
	        c3.bar();
	 
	    }
	    
	    
	}
	 class Parent {

		 
	    public static void foo() {
	        System.out.println("Inside foo method in parent class");
	    }
	 
	    public void bar() {
	        System.out.println("Inside bar method in parent class");
	    }
	}
	 
	class Child extends Parent {
	    // Hiding
	    public static void foo() {
	        System.out.println("Inside foo method in child class");
	    }
	 
	    // Overriding
	    public void bar() {
	        System.out.println("Inside bar method in child class");
	    }
	}