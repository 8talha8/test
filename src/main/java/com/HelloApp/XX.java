package com.HelloApp;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;



class XX {

    /*
     * Complete the 'calculateAmount' function below.
     *
     * The function is expected to return a LONG_INTEGER.
     * The function accepts INTEGER_ARRAY prices as parameter.
     */

    public static long calculateAmount(List<Integer> prices) {
    // Write your code here
    Integer minTill=0;
    Long sum =0L;
    
    for(int i=0;i<prices.size();i++){
        if(i==0){
            sum+= prices.get(i).longValue();
            minTill=prices.get(i);
           // return sum;
        }else{
           Integer thisInt=  prices.get(i);
           Integer prevInt= prices.get(i-1);
            if(prevInt<=thisInt){
              Integer v= (thisInt-prevInt); 
              sum+=v.longValue() ;
            }else{
                 Integer v= ((thisInt-minTill)>0?(thisInt-minTill):0); 
                  sum+=v.longValue() ; 
            }
             if(minTill<prices.get(i)){
            minTill = prices.get(i);
        }
        }
       
    }
    return sum;

    }
    
    public static void main(String[] args) throws IOException {
//        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
//
//        int pricesCount = Integer.parseInt(bufferedReader.readLine().trim());
//
//        List<Integer> prices = new ArrayList<>();
//
//        for (int i = 0; i < pricesCount; i++) {
//            int pricesItem = Integer.parseInt(bufferedReader.readLine().trim());
//            prices.add(pricesItem);
//        }
    List	prices =  Arrays.asList(10,18,1,1,16,12,10,18,19, 6,6);
    
//    prices.toArray(a)addAll({1,2});
        long result = calculateAmount(prices);

      System.out.println(result);
    }

}
