package com.HelloApp;

import java.util.*;

public class KuwateCurve4 {


		static void printLBS(List<Integer> arr, int n) throws InterruptedException 
		{ 

			
			ArrayList<Integer>[] LIS = getSubSeq(arr, n); 
			Collections.reverse(arr);
			ArrayList<Integer>[] LDS = getSubSeq(arr, n); 
			
			int max = 0; 
			int maxIndex = -1; 
			int maxIndexj = -1; 
			for (int i = 0,j=n-1; i < n; i++,j--) 
			{ 
				if (LIS[i].size() + LDS[j].size() - 1 > max) 
				{ 
					max = LIS[i].size() + LDS[j].size() - 1; 
					maxIndex = i; 
					maxIndexj= j;
				} 
			} 
			System.out.println(maxIndex +"max"+ max);

			LIS[maxIndex].remove(LIS[maxIndex].size() - 1);
			Collections.reverse(LDS[maxIndexj]);
			LIS[maxIndex].addAll(LDS[maxIndexj] );
			for (int i = 0; i < LIS[maxIndex].size(); i++) 
				System.out.print(LIS[maxIndex].get(i) + " "); 

			System.out.println();
		}

		private static ArrayList<Integer>[] getSubSeq(List<Integer> lst, int n) {
			
			@SuppressWarnings("unchecked") 
			ArrayList<Integer>[] lSeq = new ArrayList[n]; 

			for (int i = 0; i < n; i++) 
				lSeq[i] = new ArrayList<>(); 

			lSeq[0].add(lst.get(0)); 

			// Compute LIS values from left to right 
			for (int i = 1; i < n; i++) 
			{ 

				// for every j less than i 
				for (int j = 0; j < i; j++) 
				{ 

					if ((lst.get(i) > lst.get(j)) && 
						lSeq[j].size() > lSeq[i].size()) 
					{ 
						for (int k : lSeq[j]) 
							if (!lSeq[i].contains(k)) 
								lSeq[i].add(k); 
					} 
				} 
				lSeq[i].add(lst.get(i)); 
			}
			return lSeq;
//			}
		} 
 
		public static void main(String[] args) throws InterruptedException 
		{ 
			Integer[] arr = {0, 0, 0, 1, 1, 2, 1, 0, 0}; 
			int n = arr.length; 
			
			List<Integer> l1 = new ArrayList<Integer>();
			Collections.addAll(l1, arr); 

			printLBS(l1, n); 
			
			Integer[] arr2 = {0, 1, 2, 3, 1, 0, 1, 2, 4, 2, 0}; 
			int n2 = arr2.length; 
			
			List<Integer> l2 = new ArrayList<Integer>();
			Collections.addAll(l2, arr2); 

			printLBS(l2, n2); 
			
			Integer[] arr3 =  {0, 1, 2, 4, 1, 1, 0, 2, 3, 4, 3, 2, 0};
			int n3 = arr3.length; 
			
			List<Integer> l3 = new ArrayList<Integer>();
			Collections.addAll(l3, arr3); 

			printLBS(l3, n3); 
			
			
//			int[] arr2 = {0, 1, 2, 3, 1, 0, 1, 2, 4, 2, 0};
//			int n2 = arr2.length; 
//
//			printLBS(Arrays.asList(arr2), n2); 
//			
//			int[] arr3 = {0, 1, 2, 4, 1, 1, 0, 2, 3, 4, 3, 2, 0};
//			int n3 = arr3.length; 
//
//			printLBS(Arrays.asList(arr3), n3); 
			
//					Input A[] = [0, 0, 0, 1, 1, 2, 1, 0, 0], n = 9
//					Output: [0, 1, 2, 1, 0]
//					Input A[] = [0, 1, 2, 3, 1, 0, 1, 2, 4, 2, 0], n = 11
//					Output: [0, 1, 2, 3, 4, 2, 0]
//					Input A[] = [0, 1, 2, 4, 1, 1, 0, 2, 3, 4, 3, 2, 0], n = 13
//					Output: [0, 1, 2, 4, 3, 2, 0]

		
		} 
	} 



